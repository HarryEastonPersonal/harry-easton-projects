import time, requests, urllib.parse, hashlib, hmac, base64

api_key = ""
api_secret = ""
api_url = "https://api.kraken.com"

def get_kraken_signature(url_path, data, secret): #creates signature for the signing verification on requests
    postdata = urllib.parse.urlencode(data)
    encoded = (str(data["nonce"]) + postdata).encode()
    message = url_path.encode() + hashlib.sha256(encoded).digest()

    mac = hmac.new(base64.b64decode(secret), message, hashlib.sha512)
    sigdigest = base64.b64encode(mac.digest()) 

    return sigdigest.decode()


def kraken_request(url_path, data, api_key, api_secret):
    headers = {"API-Key": api_key, "API-Sign": get_kraken_signature(url_path, data, api_secret)}
    response = requests.post((api_url + url_path), headers=headers, data=data)

    return response

'''
response = kraken_request("/0/private/Balance", {
    "nonce": str(int(1000 * time.time()))
}, api_key, api_secret)

print(response.json()["result"]["XETH"])


response2 = kraken_request("/0/private/TradeBalance", {
    "nonce": str(int(1000 * time.time())), 
    "asset": "USD"
}, api_key, api_secret)

print(response2.json())

response3 = kraken_request("/0/private/OpenOrders", {
    "nonce": str(int(1000 * time.time())), 
    "trades": True
}, api_key, api_secret)

print(response3.json())

response4 = kraken_request("/0/private/TradesHistory", {
    "nonce": str(int(1000 * time.time())), 
    "trades": True
}, api_key, api_secret)

print(response4.json())

limit_order_response = kraken_request("/0/private/AddOrder", {
    "nonce": str(int(1000 * time.time())),
    "ordertype": "limit", 
    "type": "sell",
    "volume": 1.25,
    "pair": "ETHUSD",
    "price": 5000
}, api_key, api_secret)

print(limit_order_response.json())

market_order_response = kraken_request("/0/private/AddOrder", {
    "nonce": str(int(1000 * time.time())),
    "ordertype": "market", 
    "type": "buy",
    "volume": 1.25,
    "pair": "ETHUSD",
    "price": 5000
}, api_key, api_secret)

print(market_order_response.json())

cancel_order_response = kraken_request("/0/private/CancelAll", {
    "nonce": str(int(1000 * time.time())),
}, api_key, api_secret)

print(cancel_order_response.json())
'''

buy_limit = 1000
sell_limit = 3000
buy_amount = 0.01
sell_amount = 0.01

while True:
    current_price = requests.get("https://api.kraken.com/0/public/Ticker?pair=ETHCAD").json()["result"]["XETHZCAD"]["b"][0]
    
    if float(current_price) < buy_limit:
        print(f"Buying {buy_amount} of ETH at {current_price}")

        market_order = kraken_request("/0/private/AddOrder", {
            "nonce": str(int(1000 * time.time())),
            "ordertype": "market", 
            "type": "buy",
            "volume": buy_amount,
            "pair": "ETHUSD"
        }, api_key, api_secret)

        if not market_order.json()["error"]:
            print("Bought ETH")
        
        else:
            print(f"Error: {market_order.json()['error']}")
    
    elif float(current_price) > sell_limit:
        print(f"Selling {sell_amount} of ETH at {current_price}")

        market_order = kraken_request("/0/private/AddOrder", {
            "nonce": str(int(1000 * time.time())),
            "ordertype": "market", 
            "type": "sell",
            "volume": sell_amount,
            "pair": "ETHUSD"
        }, api_key, api_secret)

        if not market_order.json()["error"]:
            print("Sold ETH")
        
        else:
            print(f"Error: {market_order.json()['error']}")

    else:
        print(f"Current Price: {current_price}, not buying or selling")

    time.sleep(3) 
