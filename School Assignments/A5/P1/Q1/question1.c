#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

struct node {
    int value;
    struct node *next;
    struct node *prev;
};

struct node *element(struct node *dll, int i);
struct node *add(struct node *dll, int i, int value);
struct node *delete(struct node *dll, int i);
struct node *find_last(struct node *dll);

struct node *find_last(struct node *dll) {
    struct node *ptr = dll;
    while (ptr->next != NULL) {
        ptr = ptr->next;
    }
    return ptr;
}

struct node *element(struct node *dll, int i) {
    struct node *ptr = dll;

    for (int j = 0; j < i; j++) {
        if (ptr->next == NULL) {
            return NULL;
        }
        ptr = ptr->next;
    }

    return ptr;
}

struct node *add(struct node *dll, int i, int value) {
    struct node *new_node = malloc(sizeof(struct node));
    new_node->value = value;

    if (i == 0) {
        new_node->prev = NULL;
        new_node->next = dll;
        dll->prev = new_node;

        return new_node;
    }

    else if (i < 0 || element(dll, i) == NULL) {
        struct node *last_node = find_last(dll);
        new_node->next = NULL;
        new_node->prev = last_node;
        last_node->next = new_node;

        return dll;
    }

    else {
        struct node *old_node = element(dll, i);
        struct node *before_new = old_node->prev;

        new_node->prev = old_node->prev;
        new_node->next = old_node;
        old_node->prev = new_node;
        before_new->next = new_node;

        return dll;
    }
}

struct node *delete(struct node *dll, int i) {

    if (i == 0) {
        if (dll->next == NULL) {
            return NULL;
        }

        dll->next->prev = NULL;
        return dll->next;
    }

    else if (element(dll, i) == NULL) {
        return dll;
    }

    else {
    struct node *node_to_delete = element(dll, i);
    node_to_delete->prev->next = node_to_delete->next;
    node_to_delete->next->prev = node_to_delete->prev;

    return dll;
    }
}

void printNode (struct node *dll) {
    if (dll != NULL) {
        printf("< This: %p - %d - P : %p - N : %p >\n", dll, dll->value, dll->prev, dll->next);
    } else {
        printf("Null Pointer");
    }
}

void printLL (struct node *dll) {
    struct node* ptr = dll;
    printf("---\n");
    while (ptr != NULL) {
        printNode(ptr);
        ptr = ptr->next;
    }
    printf("---\n\n");
}

int main () {

    /*
    struct node *dll = malloc(sizeof(struct node));
    struct node *dll2 = malloc(sizeof(struct node));
    struct node *dll3 = malloc(sizeof(struct node));

    dll->value = 1;
    dll->next = dll2;
    dll->prev = NULL;

    dll2->value = 2;
    dll2->next = dll3;
    dll2->prev = dll;

    dll3->value = 3;
    dll3->next = NULL;
    dll3->prev = dll2;

    printLL(dll);
    printLL(add(dll, 1, 4));
    printLL(delete(dll, 5));
    */
    
    struct node *dll = malloc(sizeof(struct node));
    dll->value = 1;
    dll->next = NULL;
    dll->prev = NULL;
    printLL(dll);
    dll = add(dll, -1, 3);
    printLL(dll);
    dll = add(dll, -1, 4);
    printLL(dll);
    dll = add(dll, 1, 2);
    printLL(dll);
    dll = add(dll, 2, 7);
    printLL(dll);
    dll = add(dll, 9, -1);
    printLL(dll);
    dll = delete(dll, 2);
    printLL(dll);
    dll = delete(dll, 0);
    printLL(dll);
    dll = delete(dll, 4);
    printLL(dll);
    dll = delete(dll, 0);
    dll = delete(dll, 0);
    dll = delete(dll, 0);
    printLL(dll);
    dll = delete(dll, 0);
    printLL(dll);
}

/* Expected Output: 

---
< This: 0x1cf8590 - 1 - P : (nil) - N : (nil) >
---

---
< This: 0x1cf8590 - 1 - P : (nil) - N : 0x1cf85b0 >
< This: 0x1cf85b0 - 3 - P : 0x1cf8590 - N : (nil) >
---

---
< This: 0x1cf8590 - 1 - P : (nil) - N : 0x1cf85b0 >
< This: 0x1cf85b0 - 3 - P : 0x1cf8590 - N : 0x1cf85d0 >
< This: 0x1cf85d0 - 4 - P : 0x1cf85b0 - N : (nil) >
---

---
< This: 0x1cf8590 - 1 - P : (nil) - N : 0x1cf85f0 >
< This: 0x1cf85f0 - 2 - P : 0x1cf8590 - N : 0x1cf85b0 >
< This: 0x1cf85b0 - 3 - P : 0x1cf85f0 - N : 0x1cf85d0 >
< This: 0x1cf85d0 - 4 - P : 0x1cf85b0 - N : (nil) >
---

---
< This: 0x1cf8590 - 1 - P : (nil) - N : 0x1cf85f0 >
< This: 0x1cf85f0 - 2 - P : 0x1cf8590 - N : 0x1cf8610 >
< This: 0x1cf8610 - 7 - P : 0x1cf85f0 - N : 0x1cf85b0 >
< This: 0x1cf85b0 - 3 - P : 0x1cf8610 - N : 0x1cf85d0 >
< This: 0x1cf85d0 - 4 - P : 0x1cf85b0 - N : (nil) >
---

---
< This: 0x1cf8590 - 1 - P : (nil) - N : 0x1cf85f0 >
< This: 0x1cf85f0 - 2 - P : 0x1cf8590 - N : 0x1cf85b0 >
< This: 0x1cf85b0 - 3 - P : 0x1cf85f0 - N : 0x1cf85d0 >
< This: 0x1cf85d0 - 4 - P : 0x1cf85b0 - N : (nil) >
---

*/ 