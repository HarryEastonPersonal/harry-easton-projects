#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void DMY_MonthDY(char *buffer, const char *str);
void MonthDY_DMY(char *buffer, const char *str);

void DMY_MonthDY(char *buffer, const char *str) {
    char copy[11];
    strcpy(copy, str); //Copies the string so the original is unchanged
    char months[12][10] = {"January  ", "February ", "March    ", "April    ", "May      ", "June     ", "July     ", "August   ", "September", "October  ", "November ", "December "};

    char *day_str = strtok(copy, "/");
    char *month_num_str = strtok(NULL, "/"); //Splits up string into the seperate calander elements
    char *year_str = strtok(NULL, "/");

    long int day = strtol(day_str, '\0', 10);
    long int month_num = strtol(month_num_str, '\0', 10); //converts calandar elements to integers
    long int year = strtol(year_str, '\0', 10);

    char *month = *(months+month_num-1);

    sprintf(buffer, "%s %2d, %4d", month, day, year);
}

void MonthDY_DMY(char *buffer, const char *str) {
    char copy[19];
    strcpy(copy, str);
    char months[12][10] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    char *month_str = strtok(copy, " ");
    char *day_str = strtok(NULL, " "); //Splits up string into seperate calandar elements
    char *year_str = strtok(NULL, " ");

    char *filler_ptr;
    long int day = strtol(day_str, '\0', 10); //converts calandar elements to integers
    long int year = strtol(year_str, '\0', 10);

    int month;
    for (int i = 0; i < 12; i++) { //Finds the corresponding month to the month number
        if (month_str[0] == (*(months+i))[0] && month_str[1] == (*(months+i))[1] && month_str[2] == (*(months+i))[2]) {
            month = i+1; //The fisrt 3 letters of every months are unique
            break;
        }
    }

    sprintf(buffer, "%.2d/%.2d/%.4d", day, month, year);
}


int main() {
char format1[12][11] = { "01/01/1970"
                       , "10/02/1763"
                       , "15/03/0044"
                       , "18/04/1982"
                       , "05/05/1789"
                       , "06/06/1944"
                       , "01/07/1867"
                       , "03/08/1492"
                       , "02/09/1945"
                       , "24/10/1917"
                       , "11/11/1918"
                       , "08/12/1980"
                       };
char format2[12][19] = { "January    1, 1970" // First Day of History (according to computers)
                       , "February  10, 1763" // France cedes Canada to England
                       , "March     15,   44" // Assassination of Julius Caesar
                       , "April     18, 1982" // Canada Achieves Sovereignty 
                       , "May        5, 1789" // French Revolution
                       , "June       6, 1944" // D-Day Landings of WWII
                       , "July       1, 1867" // Confederation of Canada
                       , "August     3, 1492" // Christopher Columbus Sets Sail for the Americas
                       , "September  2, 1945" // WWII Surrender of Japan
                       , "October   24, 1917" // October Revolution (Julian Calendar)
                       , "November  11, 1918" // WWI Armistice
                       , "December   8, 1980" // Murder of John Lennon
                       };
    
    char buffer1[12][11];
    char buffer2[12][19];

    printf ("-- DD\\MM\\YY to Month DD, YYYY\n");
    for (int i = 0; i < 12; i++) {
        printf("%s\n", format1[i]);
        DMY_MonthDY(buffer2[i], format1[i]);
        printf("-> \"%s\"\n", buffer2[i]);
    }
    
    printf ("\n\n-- Month DD, YYYY to DD\\MM\\YY\n");
    for (int i = 0; i < 12; i++) {
        printf("%s\n", format2[i]);
        MonthDY_DMY(buffer1[i], format2[i]);
        printf("-> \"%s\"\n", buffer1[i]);
    }
}