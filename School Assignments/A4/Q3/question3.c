#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

int simpleParse(const char *exp);

int simpleParse(const char *exp) {
    int expression_size = strlen(exp) + 1;
    char *copy = malloc(expression_size*sizeof(char)); //copies expression
    strcpy(copy, exp);

    long int *integers = calloc(1, sizeof(long int)); //base array of numbers
    int integers_size = 0;

    char *operators = calloc(1, sizeof(char)); //base array of operators
    int operators_size = 0;

    char *token;
    token = strtok(copy, " "); //splits the expression at each whitespace. There will always be at least 1 whitespace

    while (token != NULL) {
        if (isdigit(*token) != 0) { //only lets the numbers through
            integers_size ++;
            integers = realloc(integers, integers_size*sizeof(long int)); //reallocates memory to fit the newest number
            *(integers+integers_size-1) = strtol(token, '\0', 10);
        }

        else { //if it's not a number then it's an operator
            operators_size ++;
            operators = realloc(operators, operators_size*sizeof(char)); //reallocates memory to fit the newest operator
            *(operators+operators_size-1) = *token;
        }

        token = strtok(NULL, " ");
    }

    for (int i = 0; i < operators_size; i++) { //BEDMAS means we do all division,modulus and multiplication first
        if (operators[i] == '%') {
            integers[i+1] = integers[i] % integers[i+1]; //the new value will be to the right of the old value
            integers[i] = '\0'; //replace old value with null character
        }
        
        else if (operators[i] == '/') {
            integers[i+1] = integers[i] / integers[i+1];
            integers[i] = '\0';
        }

        else if (operators[i] == '*') {
            integers[i+1] = integers[i] * integers[i+1];
            integers[i] = '\0';
        }
    }

    for (int j = 0; j < operators_size; j++) { //Now we do addition and substraction
        int null_jumper = 0; //Skip over the null values
        if (operators[j] == '+') {
            while (integers[j+null_jumper+1] == '\0') { //the first null value has to be in the second spot so we add 1
                null_jumper ++;
            }

            integers[j+null_jumper+1] = integers[j] + integers[j+null_jumper+1];
            integers[j+null_jumper] = '\0';
        }

        else if (operators[j] == '-') {
            while (integers[j+null_jumper+1] == '\0') {
                null_jumper ++;
            }

            integers[j+null_jumper+1] = integers[j] - integers[j+null_jumper+1];
            integers[j+null_jumper] = '\0';
        }
    }

    int output = integers[operators_size]; //The final value will be in the most right position in the array
    free(integers);
    free(operators);
    free(copy);

    return output;
}



int main() {
    char exp1[] = "1 + 1"; //2
    char exp2[] = "3 + 7 - 4 * 1"; //6
    char exp3[] = "9 * 13 / 2 + 18 * 110 - 812 % 97 + 3"; //2005

    printf("%s = %d\n", exp1, simpleParse(exp1));
    printf("%s = %d\n", exp2, simpleParse(exp2));
    printf("%s = %d\n", exp3, simpleParse(exp3));
}