#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>

void removeDups(int* in, int size);

void removeDups(int* in, int size) {

    for (int i = 0; i < size; i++) {
        for (int j = i+1; j < size; j++) {

            if (in[i] == in[j]) {
                in[j] = 0;
            }
        }  
    }

    for (int i = 0; i < size; i++) { //Modified version of Bubblesort to only sort 0s
        for (int j = i + 1; j < size; j++) {

            if (in[i] == 0) {
              int temp = in[j];
              in[j] = in[i];
              in[i] = temp;  
            }
        }
    }       
}

int main () {
    int x[] = {1,9,2,2,3,3,4,2,4,5,6,6,7,8,8,9,7,6,8,9};
    int x_size = sizeof(x)/sizeof(x[0]);
    removeDups(x, x_size);

    for (int i = 0; i < x_size; i++) {
        printf("%d", *(x+i));
    }

    return 0;
}