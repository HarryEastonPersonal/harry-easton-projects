#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

void reverse(char* str);
int count(char* str);

void reverse(char* str) {
    int length = strlen(str);

    for (int i = 0; i < length/2; i++) {
        char temp = str[i];
        str[i] = str[length-i-1];
        str[length-i-1] = temp;
    }
}

int count(char* str) {
    int vowels = 0;

    while (*str != '\0') {
        char current = tolower(*str);
        if (current == 'a' || current == 'o' || current == 'e' || current == 'i' || current == 'u') {
            vowels ++;
        }

        *str++;
    }

    return vowels;
}

int main () {
    char word[] = "HeLlO";
    reverse(word);
    for (int i = 0; i < strlen(word); i++) {
        printf("%c", *(word+i));
    }

    printf("\n%d", count(word));
    
    return 0;
}