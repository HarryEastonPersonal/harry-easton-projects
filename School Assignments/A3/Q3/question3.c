#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>


int shellsort (int *array, int size, int *h_gaps, int (*subsort)(int* array, int size));
int bubblesort(int* x, int size);

int bubblesort(int* x, int size) {
    int temp;
    int swaps = 0;
    for (int i = 0; i < size; i ++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (x[j] > x[j+1]) {
                temp = x[j];
                x[j] = x[j + 1];
                x[j+1] = temp;
                swaps ++;
            }
        }
    }
    return swaps;
}

int shellsort (int *array, int size, int *h_gaps, int (*subsort)(int* array, int size)) {
    int num_of_gaps = 1;
    int i = 0;
    int last_h;
    int sub_size;
    int swaps = 0;
    
    while(*(h_gaps+i) != 1) {
        num_of_gaps++;
        i++;
    }

    for (i = 0; i < num_of_gaps; i++) { //Loop for every h gap
        int h = *(h_gaps+i);

        if (size % h == 0) {
            last_h = h;
        }
        else {
            last_h = h + 1;
        }

        for (int j = 0; j < last_h; j++) { //For each element in the subarray
            sub_size = 0;
            for (int k = j; k < size; k += h) {
                sub_size++;
            }

            int* sub_arr = malloc(sub_size * sizeof(int)); //Allocated memory to fit the subarray
            for (int z = 0; z < sub_size; z++) {
                *(sub_arr + z) = *(array + j + z*h); //Places values into the subsarray
            }

            swaps += subsort(sub_arr, sub_size); //Sorts the sub array
            for (int v = 0; v < sub_size; v++) { //Places values back into the original array
                *(array + j + v*h) = *(sub_arr + v);
            }
            free(sub_arr);
        }
    }
    swaps += subsort(array, size); //h gap of 0

    return swaps;
}


int main () {
    int h_gaps[] = {701, 301, 132, 57, 23, 10, 4, 1};
    int arr[] = {135, 529, 81, 54, 46, 605, 47, 793, 278, 323, 306, 430, 973, 286, 712, 962, 461, 81, 57,
                 325, 711, 995, 833, 222, 284, 293, 153, 224, 126, 643, 425, 400, 420, 309, 831, 6, 496, 347, 185, 583};
    int size = sizeof(arr)/sizeof(int);

    int swaps = shellsort(arr, size, h_gaps, bubblesort);
    printf("%d\n", swaps);
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }

    return 0;
}