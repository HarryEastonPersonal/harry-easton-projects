#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>


int insertionsort(int* x, int size, int (*compare)(int a, int b));
int ascending (int a, int b);
int descending (int a, int b);
void swap (int* num1, int* num2);

int insertionsort(int* x, int size, int (*compare)(int a, int b)) {
    int j;
    int i;
    int examinations = 0;

    for (i = 1; i < size; i++) {
        j = i;
        while (j > 0) {
            examinations ++;
            if (compare(*(x+j-1), *(x+j)) == true) {
                swap(x+j-1, x+j);
                j = j - 1;
            }
            else {
                break;
            }
        }
    }

    return examinations;
}

int ascending (int a, int b) {
    if (a > b) {
        return true;
    }
    else {
        return false;
    }
}

int descending (int a, int b) {
    if (a < b) {
        return true;
    }
    else {
        return false;
    }
}

void swap (int* num1, int* num2) {
    int temp = *num1;
    *num1 = *num2;
    *num2 = temp;
}


int main () {
    int x[] = {548, 845, 731, 258, 809, 522, 73, 385, 906, 891, 988, 289, 808, 128};
    int length = sizeof(x)/sizeof(int);
    int examinations = insertionsort(x, length, ascending);

    printf("%d\n", examinations);
    for (int i = 0; i < length; i++) {
        printf("%d ",*(x+i));
    }
    return 0;
}