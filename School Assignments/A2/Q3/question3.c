#include <stdio.h>
#include <assert.h>
#include <math.h>

int bubblesort(int* x, int size);

int bubblesort(int* x, int size) {
    int swaps = 0;
    int i;
    int j;

    for (i = 0; i < size; i++) {

        for (j = 0; j < size-1-i; j++) {
            if (x[j] > x[j+1]) { //if the current value is greater than the next value switch them
                swaps += 1;
                int temp = x[j+1];
                x[j+1] = x[j];
                x[j] = temp;
            }
        }
    }
    
    return swaps;
}

int main () {
    int a[] = {548, 845, 731, 258, 809, 522, 73, 385, 906, 891, 988, 289, 808, 128};
    int b[] = {100};
    printf("\n%d", bubblesort(b, 1));
    return 0;
}