#include <stdio.h>
#include <assert.h>
#include <math.h>

int juggler(int n);

int juggler(int n) {

    if (n == 1) {
        return 0;
    }

    else if (n % 2 == 0) {
        int next_term = floor(pow(n, 0.5));
        return juggler(next_term) + 1;
    }

    else {
        int next_term = floor(pow(n, 1.5));
        return juggler(next_term) + 1;
    }
}

int main () {
    printf("%d\n", juggler(10000));

    return 0;
}