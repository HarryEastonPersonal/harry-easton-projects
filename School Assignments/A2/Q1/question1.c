#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

double mean(int* x, int size);
double median(int* x, int size);
int mode(int* x, int size);

double mean(int* x, int size) {
    int count = 0;
    double total = 0;
    while (count < size) {
        total += x[count];
        count ++;
    }

    return total / size;
}

double median(int* x, int size) {

    int copy[size];
    for (int i = 0; i < size; i++) { //make copy of array to not mutate the original
        copy[i] = x[i];
    }
    
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size-1-i; j++) {
            if (copy[j] > copy[j+1]) { //Bubble sort from Question 3
                int temp = copy[j+1];
                copy[j+1] = copy[j];
                copy[j] = temp;
            }
        }
    }

    if (size % 2 != 0) {
        return copy[(size-1)/2];
    }

    else {
        double index1 = copy[size/2]; //in order to do divide the ints as deciaml these index values need to be doubles
        double index2 = copy[(size-2)/2];
        return (index1 + index2)/2;
    }
}

int mode(int* x, int size) {
    int highest_count = 0;
    int temp_mode;
    int i;
    int j;

    for (i = 0; i < size; i++) {
        int count = 0;

        for (j = 0; j < size; j++) { //loop through all values and counting their occurences
            if (x[i] == x[j]) {
                count ++;
            }
        }

        if (count > highest_count) { //keeps track of the number with the highest amount of occurences
            highest_count = count;
            temp_mode = x[i];
        }
    }

    return temp_mode;
}

int main () {
    int y[] = {8, 1, 2, 2, 4, 9, 7, 1, 1, 1, 5, 6};
    printf("%lf\n", mean(y, 12));
    printf("%lf\n", median(y, 12));
    printf("%d\n", mode(y, 12));
    return 0;
}