#include <stdio.h>
#include <assert.h>
#include <math.h>

int insertionsort(int* x, int size);

int insertionsort(int* x, int size) {
    int j;
    int current;
    int examinations = 0;

    for (int i = 1; i < size; i++) {
        current = x[i];
        j = i - 1;

        while (j >= 0) { 
            examinations += 1;
            if (x[j] > current) { //if the current value is less than the value to the left of it, move the left value to the right
                x[j+1] = x[j];
                j = j - 1; //then look at the next value to the left
                x[j+1] = current;
            }

            else {
                break;
            }
        }
    }

    return examinations;
}

int main () {
    int a[] = {548, 845, 731, 258, 809, 522, 73, 385, 906, 891, 988, 289, 808, 128};
    int b[] = {100};
    int c[] = {9, 4, 7, 1, 7, 3, 6};
    printf("\n%d", insertionsort(b, 1));
    return 0;
}