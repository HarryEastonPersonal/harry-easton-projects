#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <stdbool.h>

int LeapYearCheck (int n);

int LeapYearCheck (int n) {
    bool out;
    if (n % 4 == 0) {
        if (n % 100 == 0) {
            if (n % 400 == 0) {
                out = true;
            }
            else {
                out = false;
            }
        }
        else {
            out = true;
        }
    }
    else {
        out = false;
    }

    return out;
}

int main () {
    printf("%d", LeapYearCheck(2100));
    return 0;
}