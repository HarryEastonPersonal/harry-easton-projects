#include <stdio.h>
#include <assert.h>
#include <math.h>

float compoundInterest (float p, int a, int n);

float compoundInterest (float p, int a, int n) {
    float future_value = a*pow(1+p, n);
    return future_value;
}

int main () {
    printf("%0.2f\n", compoundInterest(0.06, 800, 2));
    return 0;
}