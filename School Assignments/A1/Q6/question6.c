#include <stdio.h>
#include <assert.h>
#include <math.h>

int FactorialWhile (int n);
int FactorialDoWhile (int n);

int FactorialWhile (int n) {
    int counter = 1;
    int factorial = 1;
    
    while (counter <= n) {
        factorial *= counter;
        counter ++;
    }
    
    return factorial;
}

int FactorialDoWhile (int n) {
     int counter = 1;
     int factorial = 1;

     do {
        factorial *= counter;
        counter ++;
     } while (counter <= n);

    return factorial;
}

int main () {
    printf("%d\n", FactorialWhile(10));
    printf("%d\n", FactorialDoWhile(10));
    return 0;
}