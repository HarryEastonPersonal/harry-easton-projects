#include <stdio.h>
#include <assert.h>
#include <math.h>

float onethird (int n);

float onethird (int n) {
    long long int sum = 0;
    int counter = 1;
    
    if (n == 0) {
        return 0; 
    }
    
    while (counter <= n) {
        sum += pow(counter, 2);
        counter ++;
    }

    float series_sum = sum / pow(n, 3);
    return series_sum;
}

int main () {
    printf("%f\n", onethird(4000));
    return 0;
}