#include <stdio.h>
#include <assert.h>
#include <math.h>

int multiples (int x, int y, int N);

int multiples (int x, int y, int N) {
    int sum = 0;
    int counter = 0;

    for (N; counter <= N; counter++) {
        if (counter % x == 0) {
            sum += counter;
        }

        else if (counter % y ==0) {
            sum += counter;
        }
    }

    return sum;
}

int main () {
    printf("%d", multiples(11, 15, 20));
    return 0;
}