#include <stdio.h>
#include <assert.h>
#include <math.h>

void mileage (void);

void mileage (void) {
    float total_fuel;
    float total_distance;
    float fuel;
    float distance;
    char more_data = 'y';

    while (more_data == 'y') {
        printf("How much fuel have you used? : ");
        scanf("%f", &fuel);
        printf("How far did you travel? : ");
        scanf("%f", &distance);
        printf("Do you have more data to enter? (y/n) : ");
        scanf(" %c", &more_data); //need a space before %c to get rid of invisible newline character

        total_fuel += fuel;
        total_distance += distance;

        if (more_data == 'n') {
            printf("Your gas milage is %0.4f", (total_distance / total_fuel));
            break;
        }
    }
}

int main () {
	mileage();
    return 0;
}