#include <stdio.h>
#include <assert.h>
#include <math.h>

int minutes (int m, int h, int d);

int minutes (int m, int h, int d) {
    int total_min;
    total_min = m + h*60 + d*24*60;
    return total_min;
}

int main () {
    printf("%d\n", minutes(0, 0, 0));
    printf("%d", minutes(30, 15, 2));
    return 0;
}